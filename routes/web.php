<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'StaticController@homePage')->name('home-page');


Route::resource('personas', 'employeeController');
Route::resource('puestos', 'positionController');
Route::resource('empleados_personas', 'employee_PositionController');
