# Empleados Demo 📋

### Pre-requisitos 📋

_php >=  7.2_

_composer_ 

_Todo se ejecuta en raiz del proyecto_

### Instalación 🔧


_Instalar dependencias_

```
  composer install
```    


### Instalar base de datos 🔧

```
  crear base y configurarla en el archivo .env  con el nombre: empleados_demo 
```

_Instalar las migraciones (si hay modificaciones en bd, actualizar las migraciones)_
```
  php artisan migrate
```

_Insertar datos:_

```
  php artisan db:seed
```

_levantar servidor de laravel_

```
  php artisan serve
```

## Construido con 🛠️

* [Laravel](https://laravel.com/) - framework PHP
* [MYSQL](https://www.mysql.com/) - Base de datos
* [Composer](https://getcomposer.org/) - Manejador de dependencias
* [Node](https://nodejs.org/es/) - Entorno de ejecución para JavaScript

---
⌨️ con ❤️ por **Rafael Jaimes**



