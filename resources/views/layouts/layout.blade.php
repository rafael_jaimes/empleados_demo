<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta charset="utf-8">
		<title>Empleados Demo</title>
		<meta name="viewport"
		content="width=device-width, initial-scale=1, user-scalable=yes">
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>
	<body> 
		<header style="margin:10px 0 0">
			@include('shared/header')
		</header>
		<div class="container-fluid" style="margin: 50px 0 0">
			@yield('content')
		</div>
	</body>
</html>