 <h2>
    Ingesar nuevo puesto
</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Error!</strong> Revise los campos obligatorios.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-info">
    {{Session::get('success')}}
    </div>
@endif
<form method="POST" action="{{ route('puestos.store') }}" >
    {{csrf_field()}}
    <div class="form-group">
    <label for="nombre">Nombre*</label>
    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" id="nombre" placeholder="Nombre">
    </div>
    <button type="submit" class="btn btn-default">Guardar</button>

</form>