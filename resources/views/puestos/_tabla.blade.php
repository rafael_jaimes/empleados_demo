<div class="table-responsive">
    @if($puestos->count())  
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Modificar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($puestos as  $key => $puesto)
            <tr>
                <th scope="row">{{ $key+=1 }}</th>
                <td>{{ $puesto->name }}</td>
                <td>
                    <a  href="{{ route('puestos.edit', $puesto->id) }}" style="width: 100%;text-align: center;display: block;"><span class="glyphicon glyphicon-pencil" aria-hidden="true" data-id="{{ $puesto->id }}"></span></a>
                </td>
                <td>
                    <form method="POST" action="{{ route('puestos.destroy', $puesto->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit"  style="width: 100%;text-align: center;display: block;background: transparent;border: unset;" class="glyphicon glyphicon-trash"  aria-hidden="true"></button>
                </form>
                
                </td>
            </tr>             
        @endforeach
        </tbody>
    </table>
    @else
    <h2>No hay registros</h2>
    @endif
</div>