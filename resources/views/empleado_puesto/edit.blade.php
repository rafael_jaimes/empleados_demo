@extends('layouts.layout')


@section('content')
  <div class="row">
    <section class="content">
      <div class="col-md-6 col-md-offset-3">
        <h2>
          Editar Asignar puesto
        </h2>
   			@if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> Revise los campos obligatorios.<br><br>
                    <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-info">
                    {{Session::get('success')}}
                </div>
            @endif
        <form method="POST" action="{{ route('empleados_personas.update', $relacion->id) }} " >
          {{csrf_field()}}
           {{ method_field('PUT') }}
            <div class="form-group">
              <label for="fecha">Empleado*</label>
              <select name="empleado" id="empleado"  class="form-control">
                  <option value="" disabled>Selecciona un empleado</option>
                  @foreach ($empleados as $empleado) 
                    @if ($empleado->id ==  $relacion->employee_id)
                      <option value="{{ $empleado->id }}" selected>{{ $empleado->name }}</option>  
                    @else
                      <option value="{{ $empleado->id }}">{{ $empleado->name }}</option>
                    @endif
                  @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="fecha">Puesto*</label>
              <select name="puesto" id="puesto"  class="form-control">
                  <option value="" disabled>Selecciona un puesto</option>
                  @foreach ($puestos as $puesto) 
                    @if ($puesto->id ==  $relacion->position_id)
                      <option value="{{ $puesto->id }}" selected>{{ $puesto->name }}</option>
                    @else
                      <option value="{{ $puesto->id }}">{{ $puesto->name }}</option>
                    @endif
                  @endforeach
              </select>
          </div>

          <button type="submit" class="btn btn-default">Guardar</button>

        </form>
      </div>
    </section>
  </div>
@endsection

