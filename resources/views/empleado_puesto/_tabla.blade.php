<div class="table-responsive">
    @if($empleados->count())  
    
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Puesto</th>
            <th>Modificar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($empleados as  $key => $employee)
            @foreach ($employee->positions as $puesto)
            <tr>
                <th scope="row">{{ $key+=1 }}</th>
                <td>{{ $employee->name }}</td>
                <td>{{ $puesto->name }}</td>
                 <td>
                    <a  href="{{ route('empleados_personas.edit', $puesto->pivot->id) }}" style="width: 100%;text-align: center;display: block;"><span class="glyphicon glyphicon-pencil" aria-hidden="true" data-id="{{ $puesto->id }}"></span></a>
                </td>
               <td>
                    <form method="POST" action="{{ route('empleados_personas.destroy', $puesto->pivot->id ) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit"  style="width: 100%;text-align: center;display: block;background: transparent;border: unset;" class="glyphicon glyphicon-trash"  aria-hidden="true"></button>
                    </form>
                
                </td>
            </tr>             
            @endforeach
        @endforeach
        </tbody>
    </table>
    @else
    <h2>No hay registros</h2>
    @endif
</div>