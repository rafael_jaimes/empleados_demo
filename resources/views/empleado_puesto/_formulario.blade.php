 <h2>
    Asignar puesto
</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Error!</strong> Revise los campos obligatorios.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-info">
    {{Session::get('success')}}
    </div>
@endif
<form method="POST" action="{{ route('empleados_personas.store') }}" >
    {{csrf_field()}}
    
    <div class="form-group">
        <label for="fecha">Empleado*</label>
        <select name="empleado" id="empleado"  class="form-control">
            <option value="" selected disabled>Selecciona un empleado</option>
            @foreach ($empleados as $empleado) 
            <option value="{{ $empleado->id }}">{{ $empleado->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="fecha">Puesto*</label>
        <select name="puesto" id="puesto"  class="form-control">
            <option value="" selected disabled>Selecciona un puesto</option>
            @foreach ($puestos as $puesto) 
            <option value="{{ $puesto->id }}">{{ $puesto->name }}</option>
            @endforeach
        </select>
    </div>
   

    <button type="submit" class="btn btn-default">Guardar</button>

</form>