@extends('layouts.layout')


@section('content')
  <div class="row">
    <section class="content">
      {{-- tabla muestra  puestos --}}
      <div class="col-md-7">
          @include('empleado_puesto._tabla')
      </div>
      {{-- formulario crea puestos --}}
      <div class="col-md-5">
          @include('empleado_puesto._formulario')
      </div>
    </section>
  </div>
@endsection

