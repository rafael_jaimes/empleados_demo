<ul class="nav nav-tabs nav-justified">
    <li role="presentation" {{ $nav == 1 ? "class=active" : '' }}><a href="{{ url('/personas') }}">Personas</a></li>
    <li role="presentation" {{ $nav == 2 ? "class=active" : '' }}><a href="{{ url('/puestos') }}">Puestos</a></li>
    <li role="presentation" {{ $nav == 3 ? "class=active" : '' }}><a href="{{ url('/empleados_personas') }}">Empleados Puestos</a></li>
</ul>