<div class="table-responsive">
    @if($empleados->count())  
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Fecha de nacimiento</th>
            <th>Modificar</th>
            <th>Eliminar</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($empleados as  $key => $empleado)
            <tr>
                <th scope="row">{{ $key+=1 }}</th>
                <td>{{ $empleado->name }}</td>
                <td>{{ $empleado->last_name }}</td>
                <td>{{  \Carbon\Carbon::parse($empleado->birth_date)->format('d-M-Y') }}</td>
                <td>
                    <a  href="{{ route('personas.edit', $empleado->id) }}" style="width: 100%;text-align: center;display: block;"><span class="glyphicon glyphicon-pencil" aria-hidden="true" data-id="{{ $empleado->id }}"></span></a>
                </td>
                <td>
                    <form method="POST" action="{{ route('personas.destroy', $empleado->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit"  style="width: 100%;text-align: center;display: block;background: transparent;border: unset;" class="glyphicon glyphicon-trash"  aria-hidden="true"></button>
                </form>
                
                </td>
            </tr>             
        @endforeach
        </tbody>
    </table>
    @else
    <h2>No hay registros</h2>
    @endif
</div>