@extends('layouts.layout')


@section('content')
  <div class="row">
    <section class="content">
      {{-- tabla muestra usuarios --}}
      <div class="col-md-8">
          @include('home._tabla')
      </div>
      {{-- formulario crea usuario --}}
      <div class="col-md-4">
          @include('home._formulario')
      </div>
    </section>
  </div>
@endsection

