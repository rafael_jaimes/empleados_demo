 <h2>
    Ingesar nueva persona
</h2>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
    <strong>Error!</strong> Revise los campos obligatorios.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    </div>
    @endif
    @if(Session::has('success'))
    <div class="alert alert-info">
    {{Session::get('success')}}
    </div>
@endif
<form method="POST" action="{{ route('personas.store') }}" >
    {{csrf_field()}}
    <div class="form-group">
    <label for="nombre">Nombre*</label>
    <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" id="nombre" placeholder="Nombre">
    </div>

    <div class="form-group">
    <label for="apellido">Apellido*</label>
    <input type="text" class="form-control" name="apellido" value="{{ old('apellido') }}" id="apellido" placeholder="Apellido">
    </div>

    <div class="form-group">
    <label for="fecha">Fecha de nacimiento*</label>
    <input type="date" class="form-control" name="fecha" value="{{ old('fecha') }}" id="fecha" placeholder="Fecha">
    </div>
    
   
    
    <button type="submit" class="btn btn-default">Guardar</button>

</form>