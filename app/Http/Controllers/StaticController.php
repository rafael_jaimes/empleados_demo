<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Position;

class StaticController extends Controller
{
    
    public function homePage()
    {
        
        $nav = 0;
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return view('home.index', compact("nav", "empleados", "puestos"));
    }

}
