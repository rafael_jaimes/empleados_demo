<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Position;
use App\Models\EmployeePosition;

class Employee_PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  
        $nav = 3;    
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return view('empleado_puesto.index', compact("nav", "empleados", "puestos"));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'empleado'=>'required', 'puesto'=>'required']);
        $emp = Employee::find($request->empleado);
        $emp->positions()->attach($request->puesto);


        $nav = 3;    
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('empleados_personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro guardado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $relacion = EmployeePosition::find($id);
    
        $nav = 3;    
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();

        return view('empleado_puesto.edit', compact("nav", "empleados", "puestos", "relacion"));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'empleado'=>'required', 'puesto'=>'required']);
        EmployeePosition::find($id)->update(
                     [
                        "employee_id"=>$request->empleado,
                        "position_id"=>$request->puesto
                    ]
                );
        $nav = 3;    
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('empleados_personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $e = EmployeePosition::find($id);
        $e->activo = 0;
        $e->save();
        $nav = 3;    
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('empleados_personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro guardado.');   
    }
}
