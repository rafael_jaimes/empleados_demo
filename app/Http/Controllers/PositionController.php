<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Position;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nav = 2; 
        $puestos = Position::where('activo', 1)->get();
        return view('puestos.index', compact("nav", "puestos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->validate($request,[ 'nombre'=>'required']);
         Position::create(
            [
                'name'=>$request->nombre,
            ]
        );
        
        $nav = 2;
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('puestos.index', compact("nav", "puestos"))->with('success','Registro guardado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $nav = 2;
        $position = Position::find($id);
        return view('puestos.edit', compact("nav","position"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[ 'nombre'=>'required']);
        Position::find($id)->update(['name'=>$request->nombre]);
        
        $nav = 2;
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('puestos.index', compact("nav", "puestos"))->with('success','Registro modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Position::find($id);
        $p->activo = 0;
        $p->save();    
        $nav = 2;
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('puestos.index', compact("nav", "puestos"))->with('success','Registro Eliminado.');
    }
}
