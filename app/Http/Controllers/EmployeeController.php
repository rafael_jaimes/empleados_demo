<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Position;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $nav = 1;
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return view('home.index', compact("nav", "empleados", "puestos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[ 'nombre'=>'required', 'apellido'=>'required', 'fecha'=>'required']);
        $Employee = Employee::create(
            [
                'name'=>$request->nombre,
                "last_name"=>$request->apellido,
                "birth_date"=>$request->fecha
            ]
        );
        
      /*   $Employee->positions()->attach($request->puesto);
        $Employee->save(); */

        $nav = 1;
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro guardado.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $nav = 1;
        $empleado = Employee::find($id);
        return view('personas.edit', compact("nav","empleado"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[ 'nombre'=>'required', 'apellido'=>'required', 'fecha'=>'required']);
        
        Employee::find($id)->update(
                     [
                        'name'=>$request->nombre,
                        "last_name"=>$request->apellido,
                        "birth_date"=>$request->fecha
                    ]
                );
        
      $nav = 1;
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $e = Employee::find($id);
        $e->activo = 0;
        $e->save();    
        $nav = 1;
        $empleados = Employee::where('activo', 1)->get();
        $puestos = Position::where('activo', 1)->get();
        return redirect()->route('personas.index', compact("nav", "empleados", "puestos"))->with('success','Registro Eliminado.');
    }
}
