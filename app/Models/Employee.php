<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $fillable = ['name',"last_name","birth_date"];
    
    public function positions()
    {
        return $this->belongsToMany('App\Models\Position')->withPivot('id')->wherePivot('activo', 1);
    }

}
