<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;
class Position extends Model
{
    //
    protected $fillable = ['name'];

    /* Empleados */
    public function employees()
    {
       return $this->belongsToMany('App\Models\Employee')->withPivot('id')->wherePivot('activo', 1);
    }
}

