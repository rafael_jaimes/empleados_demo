<?php

use Illuminate\Database\Seeder;
use App\Models\position;
use App\Models\Employee;
use Carbon\Carbon;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $employeesArr = array(
        array(
            'name' => 'Rafael',
            'last_name' => 'Jaimes',
            'birth_date' => '1991-04-13'
        ),
        array(
            'name' => 'Pedro',
            'last_name' => 'Jaimes',
            'birth_date' => '1991-04-13'
        ),
        array(
            'name' => 'Juan',
            'last_name' => 'Jaimes',
            'birth_date' => '1991-04-13'
        ),
        array(
            'name' => 'Mara',
            'last_name' => 'Jaimes',
            'birth_date' => '1991-04-13'
        ),
        array(
            'name' => 'Lucia',
            'last_name' => 'Jaimes',
            'birth_date' => '1991-04-13'
        )
    );

    
    public function run()
    {
        self::position();   
        self::employees();
    }

    public function position(){

      $position = Position::create([
          'name' => "Abogado"
      ]);

      $this->command->info('Tabla positions inicializada con datos!');
    }

    public function employees(){
        $_employees = [];
      foreach( $this->employeesArr as $employee ) {
        $emp = Employee::create([
          'name' => $employee['name'],
          'last_name' => $employee['last_name'],
          'birth_date' => $employee['birth_date']
        ]);
        
        $emp->positions()->attach(1);
      }
     
      $this->command->info('Tabla emple inicializada con datos!');
    }


}
